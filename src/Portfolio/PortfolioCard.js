import React from 'react';
import { Grid, Card, Divider, Icon, Image, Header } from 'semantic-ui-react';
import PortfolioModal from './PortfolioModal';

const PortfolioCard = (props) => {

  const { Project, TechStack, Description, URL, Logo, Platform, PlatformIcon } = props.PortfolioItem;
  const IMAGEPATH = './Images/';

  return (
    <Grid.Column>
    <Card className="--darktheme" fluid={true}>
      <div className="__card-logo-container">
        <Image src={`${IMAGEPATH}${Logo}`} />
      </div>
      <Card.Content>
        <Card.Header><span className="--theme-primary"><a href={URL} rel="noopener noreferrer" target='_blank'>{Project}</a></span></Card.Header>
        <Card.Meta>
          <i>Tech Stack: {TechStack.join(', ')}</i>
        </Card.Meta>
        <Card.Description>
          <p>{Description.replace(/^(.{140}[^\s]*).*/, "$1")}...</p>
        </Card.Description>
        <Divider hidden />
        <PortfolioModal PortfolioItem={props.PortfolioItem} />
      </Card.Content>
      <Card.Content extra>
        <Header as='h4'>
          <Icon className="--theme-primary" size='large' name={PlatformIcon} />
          <Header.Content className="--theme-primary">{Platform}</Header.Content>
          </Header>
      </Card.Content>
    </Card>
    </Grid.Column>
  )
  
  }

export default PortfolioCard;
