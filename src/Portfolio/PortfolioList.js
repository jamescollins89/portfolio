import React from "react";
import PortfolioCard from "./PortfolioCard";
import { Header, Container, Icon, Grid  } from 'semantic-ui-react';

const PortfolioList = (props) => {
    return (
        <Container>
            <Grid>
                <Grid.Row>
                    <Grid.Column width={8}>
                        <Header as="h2">
                            <Icon className="--theme-primary" name='star' />
                            <Header.Content className="--theme-primary">Portfolio</Header.Content>
                        </Header>
                    </Grid.Column>
                    {/* <Grid.Column textAlign='right' width={8}>
                        <Dropdown
                            text='Filter By'
                            icon='filter'
                            floating
                            labeled
                            button
                            className='icon'
                        >
                            <Dropdown.Menu>
                                <Dropdown.Header icon='code' content='Filter by Platform' />
                                <Dropdown.Divider />
                                <Dropdown.Item icon='react' text='React' />
                                <Dropdown.Item icon='wordpress' text='WordPress' />
                                <Dropdown.Item icon='magento' text='Magento' />
                                <Dropdown.Item icon='shopping bag' text='Shopify' />
                                <Dropdown.Item icon='drupal' text='Drupal' />
                            </Dropdown.Menu>
                        </Dropdown>
                        <Dropdown
                            text='Sort By'
                            icon='sort'
                            floating
                            labeled
                            button
                            className='icon'
                        >
                            <Dropdown.Menu>
                                <Dropdown.Header icon='tags' content='Sort By Tag' />
                                <Dropdown.Divider />
                                <Dropdown.Item icon='tag' text='Project Name' />
                                <Dropdown.Item icon='code' text='Platform' />
                                <Dropdown.Item icon='rocket' text='Launch Date' />
                            </Dropdown.Menu>
                        </Dropdown>
                    </Grid.Column> */}
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column>
                        <Grid stackable columns={3}>
                            {props.PortfolioItems.map((item, i) => {
                                return (
                                    <PortfolioCard key={i} PortfolioItem={item} />
                                )
                            })}
                        </Grid>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Container>
    )
}

export default PortfolioList;