import React from 'react';
import { Button, Header, Image, Modal, Grid, Divider } from 'semantic-ui-react';
import Slider from "react-slick";

const PortfolioModal = (props) => {

    const { Project, TechStack, Description, URL, Logo, AdditionalImages, Platform, PlatformIcon } = props.PortfolioItem;
    const IMAGEPATH = './Images/';

    const SLIDERSETTINGS = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
    };

    return (
        <Modal className='__portfolio-modal' trigger={<Button primary content="More Details" icon='arrow right' labelPosition='right' />} closeIcon>
            <Modal.Header>
                <Grid verticalAlign='middle'>
                    <Grid.Row>
                        <Grid.Column width={8}>
                            {Project}
                        </Grid.Column>
                        <Grid.Column textAlign='right' width={8}>
                            <Button primary content={Platform} icon={PlatformIcon} labelPosition='left' />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Modal.Header>
            <Modal.Content>
                <Grid stackable columns={2}>
                    <Grid.Row>
                        <Grid.Column>
                            <Slider {...SLIDERSETTINGS}>
                                <div>
                                    <div className="__card-logo-container">
                                        <Image src={`${IMAGEPATH}${Logo}`} />
                                    </div>
                                </div>
                                {AdditionalImages ?

                                AdditionalImages.map( (image, i) => {
                                    return (
                                        <Image wrapped key={i} size='medium' src={image} />
                                    )
                                })
                                
                                : null }
                                
                            </Slider>
                        </Grid.Column>
                        <Grid.Column>
                            <Modal.Description>
                                <Header><span className='--theme-primary'>About this project</span></Header>
                                <p><i>Tech Stack: {TechStack.join(', ')}</i></p>
                                <p>{Description}</p>
                            </Modal.Description>
                            <Divider hidden />
                            <Button primary content="View Site" as="a" href={URL} target="blank" icon='arrow right' labelPosition='right' />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Modal.Content>
        </Modal>
    )
}

export default PortfolioModal;