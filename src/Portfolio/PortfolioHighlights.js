import React from "react";
import PortfolioCard from "./PortfolioCard";
import { Header, Icon, Grid } from 'semantic-ui-react';

const PortfolioHighlights = (props) => {

    return (
        <Grid>
            <Grid.Row>
                <Grid.Column>
                    <Header as="h2">
                        <Icon className="--theme-primary" name='star' />
                        <Header.Content className="--theme-primary">Portfolio Highlights</Header.Content>
                    </Header>
                </Grid.Column>
            </Grid.Row>
            <Grid.Row>
                <Grid.Column>
                    <Grid stackable columns={3}>
                        {props.PortfolioItems.sort(function (item) { return item.Highlight === true }).slice(0, 3).map((item, i) => {
                            return <PortfolioCard key={i} PortfolioItem={item} />
                        })}
                    </Grid>
                </Grid.Column>
            </Grid.Row>
        </Grid>
    )
}

export default PortfolioHighlights;