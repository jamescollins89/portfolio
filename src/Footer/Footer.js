import React from "react";
import { Container, Grid, Icon, Menu } from 'semantic-ui-react';

const Footer = () => {
    return (
        <footer className="__footer">
            <Container>
                <Grid textAlign='center' verticalAlign='middle'>
                    <Grid.Row>
                        <Grid.Column mobile={16} tablet={6} computer={6} largeScreen={6} widescreen={6}>
                            <Menu.Item link href='tel:07896255772'><Icon circular name='phone' /> 07896255772</Menu.Item>
                        </Grid.Column>
                        <Grid.Column mobile={16} tablet={6} computer={6} largeScreen={6} widescreen={6}>
                            <Menu.Item link href='mailto:jamescollins89@hotmail.co.uk'><Icon circular name='mail' /> jamescollins89@hotmail.co.uk</Menu.Item>
                        </Grid.Column>
                        <Grid.Column mobile={16} tablet={4} computer={4} largeScreen={4} widescreen={4}>
                            <Menu.Item link href='https://www.linkedin.com/in/james-collins-a930a750/' target='_blank'><Icon size='large' name='linkedin' /></Menu.Item>
                            <Menu.Item link href='https://www.bitbucket.com/jamescollins89' target='_blank'><Icon size='large' name='bitbucket' /></Menu.Item>
                            <Menu.Item link href='https://stackoverflow.com/users/3845699/james-collins' target='_blank'><Icon size='large' name='stack overflow' /></Menu.Item>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Container>
        </footer>
    )
}

export default Footer;