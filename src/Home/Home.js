import React from "react";
import { Header, Divider, Grid, Container, Button, Menu, Icon } from 'semantic-ui-react';
import { Link } from "react-router-dom";
import PortfolioHighlights from "../Portfolio/PortfolioHighlights";
import homeBackground from "../Images/homepage-background.jpg";

const Home = (props) => {
    return (
        <React.Fragment>
            <Grid className="--full-height __home-intro"
                style={{
                    backgroundImage: `linear-gradient(to bottom, rgba(0,0,0, 0.7), rgba(0,0,0, 0.8)),
            url(${homeBackground})`, backgroundSize: "cover"
                }} verticalAlign='middle'>
                <Grid.Row>
                    <Grid.Column>
                        <Header as='h1' className="--xl-header"><span className="--theme-primary">React and WordPress Developer.</span></Header>
                        <p className="--lg-text">Hi, I'm James Collins, a Frontend Developer with several years experience working in the Web Development and Graphic Design Industry. I have worked at several digital agencies and have also worked as a contractor. I have worked with clients of all sizes ranging from local florists to global fashion brands.<br /><br />
                        Having worked on a variety of platforms and frameworks, I enjoy building WordPress based websites and creating Single Page Applications with React. Please take a look at my Portfolio to see some examples of my work and the different platforms I have worked with.</p>
                        <Divider hidden />
                        <Menu.Item link href='https://www.linkedin.com/in/james-collins-a930a750/' target='_blank'><Button className="--linkedin-button" content='LinkedIn Profile' icon='linkedin' labelPosition='left' /></Menu.Item>
                        <Button primary as={Link} to="/portfolio" content='View my Portfolio' icon='right arrow' labelPosition='right' />
                    </Grid.Column>
                </Grid.Row>
            </Grid>
            <Container>
                <PortfolioHighlights PortfolioItems={props.PortfolioItems} />
                <Grid>
                    <Grid.Row>
                        <Grid.Column textAlign='center'>
                            <Button className="--home-portfolio-button" primary as={Link} to="/portfolio" content='View Full Portfolio' icon='right arrow' labelPosition='right' />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <Header as="h2">
                                <Icon className="--theme-primary" name='th list' />
                                <Header.Content className="--theme-primary">Skillset</Header.Content>
                            </Header>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <Grid padded className="__skills-grid">
                                {props.Skills.Skillset.map((skill, i) => (
                                    <Grid.Column key={i} verticalAlign='middle' textAlign='center' fluid='true'>
                                        <Icon className='--theme-primary' size='big' name={skill.Icon ? skill.Icon : 'code'} />
                                        <Header as='h4' className='--theme-primary'>{skill.Title}</Header>
                                    </Grid.Column>
                                ))}
                            </Grid>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Container>
        </React.Fragment>
    )
}

export default Home;