import React, {useEffect} from "react";
import { Link } from "react-router-dom";
import { Grid, Menu, Button, Image, Header, Icon } from 'semantic-ui-react';
import developer from '../Images/developer.svg';

const Navbar = () => {

    useEffect(() => {

        /*
            Sticky Header
        */
        const navbar = document.querySelector('._navbar');

        // OnScroll event handler
        const onScroll = () => {

            // Get scroll value
            const scroll = document.documentElement.scrollTop

            // If scroll value is more than 0 - add class
            if (scroll > 0) {
                navbar.classList.add("_pinned");
            } else {
                navbar.classList.remove("_pinned");
            }
        }

        // Use either onScroll or throttledOnScroll
        window.addEventListener('scroll', onScroll);

        /*
            Responsive Menu
        */
        const MENUTOGGLE = document.querySelector('.__menu-toggle');
        const MENUITEM = document.querySelectorAll('.menu .item');
        const MENUCONTAINER = document.querySelector('._navbar .menu');

        MENUTOGGLE.addEventListener('click', () => {
            if (MENUCONTAINER.classList.contains('active')){
                MENUCONTAINER.classList.remove('active');
                MENUTOGGLE.classList.remove('active');
            } else {
                MENUCONTAINER.classList.add('active');
                MENUTOGGLE.classList.add('active');
            }
        });

        MENUITEM.forEach(elem => elem.addEventListener('click', () => {
            MENUCONTAINER.classList.remove('active');
            MENUTOGGLE.classList.remove('active');
        }));

        const MENUBUTTON = document.querySelector('.menu .item .button');
        MENUBUTTON.addEventListener('click', () => {
            document.querySelector('footer').scrollIntoView();
        });

    }); // End of UseEffect

    return (
        <Grid className="_navbar" verticalAlign='middle'>
            <Grid.Row>
                <Grid.Column mobile={13} tablet={6} computer={6} largeScreen={6} widescreen={6}>
                    <Menu.Item as={Link} to="/">
                        <Header as='h2' className='__navbar-header'>
                            <Image src={developer} />
                            <Header.Content>James Collins<br /><span className='--theme-primary'>Frontend Developer</span></Header.Content>
                        </Header>
                    </Menu.Item>
                </Grid.Column>
                <Grid.Column className='__menu-container' mobile={3} tablet={10} computer={10} largeScreen={10} widescreen={10}>
                    <Icon className='__menu-toggle' name='bars' size='large' />
                    <Menu stackable floated='right' compact>
                        <Menu.Item as={Link} to="/">Home</Menu.Item>
                        <Menu.Item as={Link} to="/portfolio">Portfolio</Menu.Item>
                        <Menu.Item as={Link} to="/skills">Skills</Menu.Item>
                        <Menu.Item as={Link} to="/about">About</Menu.Item>
                        <Menu.Item className='--has-button'>
                            <Button>Contact</Button>
                        </Menu.Item>
                    </Menu>
                </Grid.Column>
            </Grid.Row>
        </Grid>
    )
}

export default Navbar;