import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from 'react-router-dom';
import './Scss/App.scss';
import { Container } from 'semantic-ui-react';
import Navbar from './Navbar/Navbar';
import PortfolioItems from './Data/PortfolioItems';
import PortfolioList from './Portfolio/PortfolioList';
import Home from './Home/Home';
import Skills from './Data/Skills';
import SkillsList from './Skills/SkillsList';
import About from './About/About';
import Footer from './Footer/Footer';

const App = () => {
    return (
        <Container fluid>
            <Router>
                <Navbar />
                <main>
                <Switch>
                    <Route exact path='/' render={() =>
                        <Home PortfolioItems={PortfolioItems} Skills={Skills} />
                    } />
                    <Route path='/portfolio' render={() =>
                        <PortfolioList PortfolioItems={PortfolioItems} />
                    } />
                    <Route path='/skills' render={() =>
                        <SkillsList Skills={Skills} />
                    } />
                    <Route path='/about' render={() =>
                        <About />
                    } />
                </Switch>
                </main>
            </Router>
            <Footer />
        </Container>
    )
}

export default App;