[
    {
        "Project": "Teenage Cancer Trust",
        "TechStack": ["React", "ES6 JavaScript", "Semantic UI"],
        "Description": "The Teenage Cancer Trust needed a new donations portal as their existing one was old fashioned and unintuitive which was leading to a large bounce rate and a drop in donations. I was tasked with creating a modern, fast and easy to use Single Page Application to increase the User Experience and reduce the bounce rate. I decided to use React as it seemed the perfect solution, I used the recently added React Hooks to assist with the functionality paired with Semantic UI React to streamline the UI build.",
        "URL": "https://donate.teenagecancertrust.org/",
        "Logo": "tct.jpg",
        "Platform": "React SPA",
        "PlatformIcon": "react",
        "Highlight": true
    },
    {
        "Project": "Proofer",
        "TechStack": ["React", "Apollo", "GraphQL", "Semantic UI"],
        "Description": "I worked as a contractor for Proofer, a tech startup based in London. Proofer is a large React based Application for Social Media Management. I collaborated with the lead developer to add new functionality to the site as well as refactoring old code and improving the UI. Working on this app exposed me to a lot of exciting tools I had wanted to use for a long time such as Apollo and GraphQL.",
        "URL": "https://proofer.io/",
        "Logo": "proofer-logo.svg",
        "Platform": "React SPA",
        "PlatformIcon": "react",
        "Highlight": true
    },
    {
        "Project": "Chanel Eyewear",
        "TechStack": ["SCSS", "JavaScript", "jQuery", "ScrollMagic", "NPM Scripts"],
        "Description": "Chanel Eyewear was one of the first Magento 2 sites I built. Using a custom CSS framework based on Bootstrap, I built out the entire Frontend of the site. Often having to collaborate with the Backend team to build the custom components such as the Virtual Try On feature or the bespoke Checkout page. Each month there would be a new campaign story for the latest products and this meant creating bespoke landing pages often requiring a lot of animation and scroll effects. I used a variety of libraries to build these pages, especially ScrollMagic paired with GreenSock Animations.",
        "URL": "https://www.chanel.com/en_GB/fashion/sunglasses/",
        "Logo": "chanel-logo.svg",
        "Platform": "Magento 2",
        "PlatformIcon": "magento",
        "Highlight": true
    },
    {
        "Project": "Margate Caves",
        "TechStack": ["HTML", "SCSS", "JavaScript", "jQuery", "ACF"],
        "Description": "Margate Caves needed a new website for the launch of their re-opening. With a tight deadline, I created a WordPress website for them, using Advanced Custom Fields to provide flexibility for managing the content where needed.",
        "URL": "https://www.margatecaves.co.uk/",
        "Logo": "margate-caves-logo.png",
        "Platform": "WordPress",
        "PlatformIcon": "wordpress"
    },
    {
        "Project": "BMW Racedays",
        "TechStack": ["HTML", "CSS", "JavaScript", "PHP", "Gulp"],
        "Description": "BMW Racedays is a WordPress site using the WooCommerce and Tribe Events plugins. My work on this project involved creating several templates written in PHP, pulling in functionality from the different plugins where required. After all the templates were created I styled the bespoke theme using CSS and using JavaScript to add further functionality where required.",
        "URL": "https://www.bmwracedays.co.uk/",
        "Logo": "bmw-racedays-logo.png",
        "Platform": "WordPress",
        "PlatformIcon": "wordpress"
    },
    {
        "Project": "Adept",
        "TechStack": ["HTML", "SCSS", "jQuery", "SVG Animation"],
        "Description": "For this site I created a bespoke WordPress theme and also developed an SVG/JavaScript based animation for the home page",
        "URL": "https://www.adept.co.uk/",
        "Logo": "adept-logo.jpg",
        "Platform": "WordPress",
        "PlatformIcon": "wordpress"
    },
    {
        "Project": "Aston Lark",
        "TechStack": ["SCSS", "Bootstrap", "Twig", "Grunt"],
        "Description": "Aston Lark is a multitheme Drupal 8 site using a Bootstrap based UI kit. My work on this project was building the main website theme that could then be extended and easily updated for each subsite to inherit the parent theme and apply child theme branding. I also had to work closely with the Backend team to create the custom components for building the site, often using twig templates to achieve this.",
        "URL": "https://www.astonlark.com/",
        "Logo": "aston-lark-logo.svg",
        "Platform": "Drupal",
        "PlatformIcon": "drupal"
    },
    {
        "Project": "Brora",
        "TechStack": ["SCSS", "JavaScript", "PHP", "NPM Scripts"],
        "Description": "Brora was another large Magento 2 project for an International fashion client. Using my knowledge from previous projects I was able to create a UI Starter Kit that ensured the site build was fast and reliable. On this project I used a Content Manager plugin that required some templating to be coded in PHP.",
        "URL": "https://www.brora.co.uk/",
        "Logo": "brora-logo.png",
        "Platform": "Magento 2",
        "PlatformIcon": "magento"
    },
    {
        "Project": "Hayford & Rhodes",
        "TechStack": ["Liquid", "SCSS", "JavaScript"],
        "Description": "Hayford & Rhodes is a Shopify site with a customized theme and bespoke page templates. Being the sole developer on this project I had to create the page templates using the powerful Liquid templating language. I had to use a few plugins on this site and often would need to extend the functionality of these plugins usually with JavaScript or Liquid.",
        "URL": "https://www.hayfordandrhodes.co.uk/",
        "Logo": "hayford-and-rhodes-logo.svg",
        "Platform": "Shopify",
        "PlatformIcon": "shopping bag"
    },
    {
        "Project": "JM Finn",
        "TechStack": ["HTML", "SCSS", "JavaScript"],
        "Description": "JM Finn is a large Drupal 8 website. My work on this project involved working with the backend developers to create new templates for custom taxonomies and views. Once the templating was complete I would then add the necessary styling and interaction to these pages.",
        "URL": "https://www.jmfinn.com/",
        "Logo": "jmfinn-logo.png",
        "Platform": "Drupal",
        "PlatformIcon": "drupal"
    },
    {
        "Project": "Lightfoot Travel",
        "TechStack": ["SCSS", "Bootstrap", "Twig", "Grunt"],
        "Description": "Lightfoot Travel is a large Drupal 8 site with many custom built components. My responsibilities on this project were working with the in-house Backend team and contractors to build the twig template. I would then style the components using a Bootstrap based UI kit.",
        "URL": "https://www.lightfoottravel.com/",
        "Logo": "lightfoot-travel.svg",
        "Platform": "Drupal",
        "PlatformIcon": "drupal"
    },
    {
        "Project": "Rose Uniacke",
        "TechStack": ["SCSS", "PHP", "JavaScript", "NPM Scripts"],
        "Description": "The Rose Uniacke project involved migrating from a Drupal Commerce site to the Magento 2 platform. Working with the Backend team we refactored code where possible and created bespoke templates for content powered by the BlackBird Content Manager extension.",
        "URL": "https://www.roseuniacke.com/",
        "Logo": "rose-uniacke-logo.svg",
        "Platform": "Magento 2",
        "PlatformIcon": "magento"
    }
]