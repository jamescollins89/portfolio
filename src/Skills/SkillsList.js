import React from "react";
import { Container, Grid, Icon, Header, Button, Divider } from 'semantic-ui-react';

const SkillsList = (props) => {

    return (
        <Container>
            <Grid>
                <Grid.Row>
                    <Header as="h2">
                        <Icon className="--theme-primary" name='th list' />
                        <Header.Content className="--theme-primary">Skillset</Header.Content>
                    </Header>
                </Grid.Row>
                <Grid.Row>
                    <Grid padded className="__skills-grid">
                        {props.Skills.Skillset.map((skill, i) => (
                            <Grid.Column key={i} verticalAlign='middle' textAlign='center' fluid='true'>
                                <Icon className='--theme-primary' size='big' name={skill.Icon ? skill.Icon : 'code'} />
                                <Header as='h4' className='--theme-primary'>{skill.Title}</Header>
                            </Grid.Column>
                        ))}
                    </Grid>
                </Grid.Row>
                <Divider hidden />
                <Grid.Row>
                    <Header as="h2">
                        <Icon className="--theme-primary" name='th list' />
                        <Header.Content className="--theme-primary">Platforms I work with</Header.Content>
                    </Header>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column>
                        <Grid stackable columns={3} className="__skills-platform-container">
                            {props.Skills.Platforms.map((platform, i) => {
                                return (
                                    <Grid.Column key={i}>
                                        <Grid className="__skills-platform-grid" columns={2} verticalAlign='middle'>
                                            <Grid.Row>
                                                <Grid.Column width={4} textAlign='center'>
                                                    <Icon className='--theme-primary' name={platform.Icon} size='big' />
                                                </Grid.Column>
                                                <Grid.Column width={12} textAlign='left'>
                                                    <Header as='h3'>{platform.Title}</Header>
                                                    {/* <p>{platform.Description}</p> */}
                                                    {/* {platform.Examples ?
                                                        platform.Examples.map((example, i) => {
                                                            return (
                                                                <Button primary as="a" href={example.URL} target="blank" content={example.Project} icon='arrow right' labelPosition='right' key={i} />
                                                            )
                                                        }) : null} */}
                                                </Grid.Column>
                                            </Grid.Row>
                                        </Grid>
                                    </Grid.Column>
                                )
                            })}
                        </Grid>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Container>
    )
}

export default SkillsList;