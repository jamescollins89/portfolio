import React from "react";
import { Container, Grid, Image, Header, Icon } from 'semantic-ui-react';

const RecentlyPlayed = () => {
    return (
        <Header>
            <Icon className="--spotify-green" name='spotify' />
            <Header.Content className="--spotify-green">Recently Played</Header.Content>
        </Header>
    )
};

export default RecentlyPlayed;