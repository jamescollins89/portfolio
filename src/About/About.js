import React from "react";
import { Container, Grid, Image, Header, Icon, Divider, Menu, Button } from 'semantic-ui-react';
import ProfilePicture from "../Images/profile-picture.jpg";
import AsOneLogo from "../Images/asone-logo.svg";
import GALogo from "../Images/g-a-logo.jpg";
import IFLogo from "../Images/if-consultancy-logo.png";
import ProoferLogo from "../Images/proofer-logo.svg";

const About = () => {

    return (
        <Container>
            <Grid stackable>
                <Grid.Row>
                    <Grid.Column width={11}>
                        <Grid>
                            <Header as="h2">
                                <Icon className="--theme-primary" name='user' />
                                <Header.Content className="--theme-primary">Companies I've worked for</Header.Content>
                            </Header>
                            <Grid.Row style={{ padding: '0' }}>
                                <Grid.Column width={16}>
                                    <Divider />
                                </Grid.Column>
                            </Grid.Row>
                            <Grid stackable>
                            <Grid.Row>
                                <Grid.Column width={3}>
                                    <Image circular src={GALogo} size='small' />
                                </Grid.Column>
                                <Grid.Column width={13}>
                                    <Header as='h2' className='--theme-primary'>Graphic Alliance - Senior Frontend Developer</Header>
                                    <p>At Graphic Alliance I worked on a range of tech stacks including React SPAs, Shopify, Drupal and Magento 2. I often led the frontend team on a number of projects, showcasing new technologies and approaches to senior members of the team as well as guiding the junior developers.</p>
                                    <p>On my most recent project I was tasked with creating a solution to a donations portal that was experiencing a high bounce rate and user drop off. I chose to rebuild the portal using React and collaborated with the team on how we could use React Hooks, the Fetch API and ES6 functions for the build and Semantic UI for the layouts.</p>
                                    <p>My main responsibilities at Graphic Alliance:</p>
                                    <ul>
                                        <li>Transforming Sketch layouts into clean coded, fully responsive apps</li>
                                        <li>Using a modern approach and utilising best practises to efficiently write scalable code</li>
                                        <li>Being mindful of accessibility, performance and search engine optimisation techniques</li>
                                        <li>Working with a variety of APIs from monolithic PHP frameworks to REST APIs</li>
                                        <li>Collaborating with backend developers and devops engineers</li>
                                        <li>Using version control with git and Bitbucket to manage source control and CI/CD with Bitbucket pipelines</li>
                                        <li>Peer reviewing Pull Requests and Pair Working on projects where appropriate</li>
                                        <li>Performing Cross-browser testing and launch checklists</li>
                                        <li>Contributing to sprint planning, standups and retrospective meetings</li>
                                    </ul>
                                    <p>Working on the Magento 2 team I worked on projects for high-end fashion brands such as Chanel, Brora and Holland & Holland. The majority of my workload on these projects was developing the UI using Sass, JavaScript and jQuery. I would occasionally need to use JavaScript animation libraries such as ScrollMagic and Green Sock for some of the more bespoke pages that involved a lot of animation effects.</p>
                                </Grid.Column>
                            </Grid.Row>
                            <Divider />
                            <Grid.Row>
                                <Grid.Column width={3}>
                                    <Image circular src={ProoferLogo} size='small' />
                                </Grid.Column>
                                <Grid.Column width={13}>
                                    <Header as='h2' className='--theme-primary'>Proofer - Contractor</Header>
                                    <p>Proofer is a React based SPA for scheduling content across multiple social media accounts. I worked on this project to develop my React knowledge and learn new skills. I would collaborate with the lead developer on adding new functionality to the app, often this would involve using React, GraphQL and Apollo to send and receive data from the backend API. I would then build the UI with Semantic UI React.</p>
                                </Grid.Column>
                            </Grid.Row>
                            <Divider />
                            <Grid.Row>
                                <Grid.Column width={3}>
                                    <Image src={AsOneLogo} size='small' />
                                </Grid.Column>
                                <Grid.Column width={13}>
                                    <Header as='h2' className='--theme-primary'>AsOne - WordPress and e-Commerce Developer</Header>
                                    <p>At AsOne I was the sole developer on all WordPress, Opencart and Magento 1 projects. It was my responsibility to turn Photoshop/Adobe XD designs into a website theme, occasionally I would create the designs myself. Being the only developer I would often have to develop functionality on the backend of the site too and would need to use PHP to achieve this.<br /><br />
                                    While I was at AsOne I taught myself how to use SCSS, Version Control and task runners such as Grunt and Gulp to improve my Frontend skillset.<br /><br />
                                    I would assist with any technical support for clients such as email setup, domain transfer and management. It was also my role to maintain the web and email hosting server for all of our clients.
                                    </p>
                                </Grid.Column>
                            </Grid.Row>
                            <Divider />
                            <Grid.Row>
                                <Grid.Column width={3}>
                                    <Image src={IFLogo} size='small' />
                                </Grid.Column>
                                <Grid.Column width={13}>
                                    <Header as='h2' className='--theme-primary'>IF Consultancy - Developer</Header>
                                    <p>This was my first role as a developer, at first I would build static sites with HTML and CSS but before long I moved onto WordPress and started to create themes needing to use PHP, JavaScript and jQuery to achieve this.</p>
                                </Grid.Column>
                            </Grid.Row>
                            </Grid>
                        </Grid>
                    </Grid.Column>
                    <Grid.Column className='__about-me-column' width={4} floated='right' textAlign='center'>
                        <Grid.Column>
                            <Image circular src={ProfilePicture} size='small' centered />
                        </Grid.Column>
                        <Grid.Column>
                            <Header as='h2' className='--theme-primary' style={{ marginTop: '1em' }}>About Me</Header>
                            <p>I have been a web developer for almost 10 years, starting out as Full Stack and eventually specialising as a Frontend Developer. I have worked with a variety of CMS platforms including WordPress, Drupal and Magento but have more recently been focusing on JS Frameworks and have experience with React and Vue.<br /><br />I am passionate about what I do and genuinely enjoy coding, especially when challenging myself to improve my knowledge and learn new techniques. I have been to a number of developer conferences and meetups including <a href="https://frontendlondon.co.uk/">Front End London</a>, <a href="https://githubconstellation.com/">Github Constellation</a> and Magento monthly.<br /><br />
                            In my spare time I enjoy keeping fit, either at the gym or with my running club. Last year I ran the Hell Runner half marathon and also completed a 105 mile triathlon across Scotland. I also like going to pub quizzes, seeing live music and travelling the world.</p>
                            <Divider hidden />
                            <Menu.Item link href='https://www.instagram.com/jamesbloob' target='_blank'><Button className="--instagram-button" content='View my Instagram' icon='instagram' labelPosition='left' /></Menu.Item>
                        </Grid.Column>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Container>
    )
}

export default About;